﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        private void Page1_OnAppearing(object sender, EventArgs e)
        {
            UserDialogs.Instance.Toast("Scroll to learn more...", dismissTimer: TimeSpan.MaxValue);

        }

        private void Page1_OnDisappearing(object sender, EventArgs e)
        {
            base.OnDisappearing();
        }
    }
}