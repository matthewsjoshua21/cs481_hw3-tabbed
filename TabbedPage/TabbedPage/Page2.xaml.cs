﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : CarouselPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        private void Page2_OnAppearing(object sender, EventArgs e)
        {
            UserDialogs.Instance.Toast("Swipe...", TimeSpan.MaxValue);

        }

        private void Page2_OnDisappearing(object sender, EventArgs e)
        {
            base.OnDisappearing();
        }
    }
}