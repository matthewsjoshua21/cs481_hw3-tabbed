﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        private void LaunchVideo1(object sender, EventArgs e)
        { 
            Launcher.OpenAsync("https://www.youtube.com/watch?v=jBuItHrmH3k");
        }
        private void LaunchVideo2(object sender, EventArgs e)
        {
            Launcher.OpenAsync("https://www.youtube.com/watch?v=YeiXnyvo0d4");
        }
        private void LaunchVideo3(object sender, EventArgs e)
        {
            Launcher.OpenAsync("https://www.youtube.com/watch?v=WdAyO1_0noM");
        }
        private void LaunchVideo4(object sender, EventArgs e)
        {
            Launcher.OpenAsync("https://www.youtube.com/watch?v=dg_vqGgXivM");
        }
        private void LaunchVideo5(object sender, EventArgs e)
        {
            Launcher.OpenAsync("https://www.youtube.com/watch?v=73Od7Oo-YL4");
        }
        private void LaunchVideo6(object sender, EventArgs e)
        {
            Launcher.OpenAsync("https://www.youtube.com/watch?v=xhbg2nYCKms");
        }
        private void Page3_OnAppearing(object sender, EventArgs e)
        {
            UserDialogs.Instance.Toast("Click a link, listen to some good 'ol tunes...", TimeSpan.MaxValue);

        }
        private void Page3_OnDisappearing(object sender, EventArgs e)
        {
            base.OnDisappearing();
        }



    }
}